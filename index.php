<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>cropper.js</title>
    <!--Stylesheet-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/cropper.min.css">
    <link rel="stylesheet" href="css/main.css">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
</head>
<body>
  <div class="container" id="crop-avatar">

    <!-- Current avatar -->
    	
    <div class="avatar-view" title="Change the avatar">
      <img src="img/picture.jpg" alt="Avatar">
    </div>

    <!-- Cropping modal -->
    <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <form class="avatar-form" action="crop.php" enctype="multipart/form-data" method="post">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" id="avatar-modal-label">Change Avatar</h4>
            </div>
            <div class="modal-body">
              <div class="avatar-body">

                <!-- Upload image and data -->
                <div class="avatar-upload">
                  <input type="hidden" class="avatar-src" name="avatar_src">
                  <input type="hidden" class="avatar-data" name="avatar_data">
                  <label for="avatarInput">Local upload</label>
                  <input type="file" class="avatar-input" id="avatarInput" name="avatar_file">
                </div>

                <!-- Crop and preview -->
                <div class="row">
                  <div class="col-md-9">
                    <div class="avatar-wrapper"></div>
                  </div>
                  <div class="col-md-3">
                    <div class="avatar-preview preview-lg"></div>
                    <div class="avatar-preview preview-md"></div>
                    <div class="avatar-preview preview-sm"></div>
                  </div>
                </div>

                <div class="row avatar-btns">
                  <div class="col-md-9">
                    <div class="btn-group">
                      <button type="button" class="btn btn-primary fa fa-arrows" data-method="setDragMode" data-option="move" title="Move"></button>
                      <button type="button" class="btn btn-primary fa fa-crop" data-method="setDragMode" data-option="crop" title="Crop"></button>
                    </div>
                    <div class="btn-group">
                      <button type="button" class="btn btn-primary fa fa-search-plus" data-method="zoom" data-option="0.1" title="Zoom In"></button>
                      <button type="button" class="btn btn-primary fa fa-search-minus" data-method="zoom" data-option="-0.1" title="Zoom Out"></button>
                    </div>
                    <div class="btn-group">
                      <button type="button" class="btn btn-primary fa fa-rotate-left" data-method="rotate" data-option="-45" title="Rotate Left"></button>
                      <button type="button" class="btn btn-primary fa fa-rotate-right" data-method="rotate" data-option="45" title="Rotate Right"></button>
                    </div>
                    
                    <div class="btn-group">
                      <button type="button" class="btn btn-primary fa fa-refresh" data-method="reset" title="Reset"></button>
                    </div>
                    
                    
                  </div>
                  <div class="col-md-3">
                    <button type="submit" class="btn btn-primary btn-block avatar-save">Done</button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div><!-- /.modal -->

    <!-- Loading state -->
    <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
  </div>

<footer><p>&copy; 2017 Code-Ninjas , All Right Reserved.</p></footer>

<!-- Javascript-->
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/cropper.min.js"></script>
  <script src="js/main.js"></script>
</body>
</html>
